<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class campo extends Model
{
    protected $fillable = ['id','name','cp','town','street','number','photo'];
}
