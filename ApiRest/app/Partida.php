<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class partida extends Model
{
    protected $fillable = ['id','type','photo','name','description','date','max_players'];
}
