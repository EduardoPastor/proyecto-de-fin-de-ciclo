<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partida,DB,Redirect;

class PartidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $partida = new Partida;
        if(request()->image){
            request()->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = request()->name.'.jpg';
            request()->image->move(public_path('img/partidas'), $imageName);
            $partida->photo='img/partidas/'.$imageName;
        }
        $partida->type = $request->type;
        $partida->name = $request->name;
        $partida->description = $request->description;
        $partida->date = $request->date;
        $partida->max_players = $request->max_players;
        $partida->save();
        DB::table('campo_partida')->insert([
            'id_partida' => $partida->id,
            'id_campo' => $request->campo
           ]);
        return Redirect::to('campos')
                ->with('mensaje_error', 'Partida añadida correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Partida::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $partida = Partida::find($request->id);
        if(request()->image){
            request()->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = request()->name.'.jpg';
            request()->image->move(public_path('img/partidas'), $imageName);
            $partida->photo='img/partidas/'.$imageName;
        }
        $partida->type = $request->type;
        $partida->name = $request->name;
        $partida->description = $request->description;
        $partida->date = $request->date;
        $partida->max_players = $request->max_players;
        $partida->save();
        return Redirect::to('campos')
                ->with('mensaje_error', 'Partida editada correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Partida::destroy($id);
        return Redirect::to('campos')
                ->with('mensaje_error', 'Partida eliminada correctamente.');
    }

    public function inscripcion(Request $request)
    {
        $inscrito = DB::select('SELECT * FROM user_partida WHERE id_partida = "'. $request->idPartida . '" AND id_user = "'.$request->idUser.'"');
        if(sizeof($inscrito)!=1){
            DB::table('user_partida')->insert([
                'id_partida' => $request->idPartida,
                'id_user' => $request->idUser
               ]);
               return true;
        }else{
            DB::table('user_partida')->where('id_partida', '=', $request->idPartida, 'AND', 'id_user', '=', $request->idUser)->delete();
            return false;
        }
        
    }
}
