<?php

namespace App\Http\Controllers;
use Auth, View, Request, Redirect, Hash;
use App\User;

class AuthController extends Controller {
    public function showLogin()
    {
        if (Auth::check()){
            return Redirect::to('perfil');
        }else{
            return View::make('login');
        }
    }

    public function login()
    {
        $userdata = array(
            'username' => Request::get('username'),
            'password'=> Request::get('password')
        );
        if(Auth::attempt($userdata, Request::get('remember-me', 0))){
            $user = Auth::user();
            $user->last_ip = Request::ip();
            $user->save();
            return Redirect::to('/');
        }
        return Redirect::to('login')
                    ->with('mensaje_error', 'Usuario o contraseña incorrectas.')
                    ->withInput();
    }

    public function logOut()
    {
        Auth::logout();
        return Redirect::to('login')
                    ->with('mensaje_error', 'Has cerrado sesión');
    }

    public function showRegister()
    {
        if (Auth::check()){
            return Redirect::to('/');
        }else{
            return View::make('register');
        }
    }

    public function valid_email($str)
    {
        $matches = null;
        return (1 === preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/', $str, $matches));
    }

    public function register()
    {
        $user = new User;
        $user->username = Request::get('username');
        $user->email = Request::get('email');
        $user->password = Hash::make(Request::get('password'));
        $user->birthday = Request::get('date');
        $user->last_ip = Request::ip();
        $user->rol = 'user';
        
        if(strlen(trim(Request::get('username')))==0 || 
        strlen(trim(Request::get('email')))==0 ||
        strlen(trim(Request::get('password')))==0 || 
        strlen(trim(Request::get('date')))==0){
            return Redirect::to('register')
                        ->with('mensaje_error', 'Recuerda rellenar todos los campos.')
                        ->withInput();
        }else{
            
            if(Request::get('password')==Request::get('confirmPassword')){
                $user->save();
                return Redirect::to('/login')
                            ->with('username', Request::get('username'));
            }else{
                if(AuthController::valid_email(Request::get('email'))){
                    return Redirect::to('register')
                        ->with('mensaje_error', 'Compruebe el correo electrónico.')
                        ->withInput();
                }
                return Redirect::to('register')
                        ->with('mensaje_error', 'Las contraseñas tienen que ser iguales.')
                        ->withInput();
            }
        }
    }
    public function newPass()
    {   
        if(strlen(trim(Request::get('password')))==0 ||
           strlen(trim(Request::get('passwordTwo')))==0){
            return Redirect::to('perfil')
            ->with('mensaje_error', 'La contraseña no puede estar vacia.')
            ->withInput();  
           }else{
               if(Request::get('password')==Request::get('passwordTwo')){
                $user = Auth::user();
                $user->password = Hash::make(Request::get('password'));
                $user->save();
                return Redirect::to('perfil')
                        ->with('mensaje_error', 'Contraseña cambiada correctamente')
                        ->withInput();
               }else{
                return Redirect::to('perfil')
                ->with('mensaje_error', 'Las contraseñas tienen que coincidir.')
                ->withInput();
               }
           }
    }

    public function changeInfo(){
        $user = Auth::user();
        $user->name = Request::get('name');
        $user->surnames = Request::get('surnames');
        $user->twitter = Request::get('twitter');
        $user->facebook = Request::get('facebook');
        $user->instagram = Request::get('instagram');
        $user->team = Request::get('team');
        $user->save();
        return Redirect::to('perfil')
                ->with('mensaje_error', 'Datos cambiados correctamente.');
    }

    public function deleteUser(){
        $user = new User;
        $user = User::find(Request::get('id'));
        $user->destroy(Request::get('id'));
        return Redirect::to('perfil')
                ->with('mensaje_error', 'Usuario eliminado correctamente.');
    }

    public function changeRol(){
        $user = User::find(Request::get('id'));
        $user->rol = Request::get('rol');
        $user->save();
        return Redirect::to('usuarios')
                ->with('mensaje_error', 'Permisos cambiados correctamente.');
    }
}