<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, Redirect;

class ImageUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function imageUploadUser()
    {
        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $imageName = Auth::user()->username.'.jpg';
        request()->image->move(public_path('img/users'), $imageName);
        $user = Auth::user();
        $user->photo='img/users/'.$imageName;
        $user->save();
        return Redirect::to('perfil')
            ->with('mensaje_error', 'Imagen cambiada correctamente.');
    }

    public function imageUploadCampo()
    {
        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $imageName = request()->name.'.jpg';
        request()->image->move(public_path('img/campos'), $imageName);
        $campo = Campo::find(request()->id);
        $campo->photo='img/users/'.$imageName;
        $campo->save();
    }
}