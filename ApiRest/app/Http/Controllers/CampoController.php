<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campo;
use Redirect, DB;

class CampoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campo = new Campo;
        if(request()->image){
            request()->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = request()->name.'.jpg';
            request()->image->move(public_path('img/campos'), $imageName);
            $campo->photo='img/campos/'.$imageName;
        }
        $campo->name = $request->name;
        $campo->cp = $request->cp;
        $campo->street = $request->street;
        $campo->town = $request->town;
        $campo->number = $request->number;
        $campo->save();
        DB::table('user_campo')->insert([
            'id_user' => $request->user,
            'id_campo' => $campo->id
           ]);
        return Redirect::to('campos')
                ->with('mensaje_error', 'Campo añadido correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Campo::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $idUser = DB::table('user_campo')->select('id_user')->where('id_campo', '=', $request->campo);
        $idUser = (array) $idUser->get()[0];
        if($idUser['id_user']==$request->user || $request->rol=="admin"){
            $campo = Campo::find($request->campo);
            if(request()->image){
                request()->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);
                $imageName = request()->name.'.jpg';
                request()->image->move(public_path('img/campos'), $imageName);
                $campo->photo='img/campos/'.$imageName;
            }
            $campo->name = $request->name;
            $campo->cp = $request->cp;
            $campo->street = $request->street;
            $campo->town = $request->town;
            $campo->number = $request->number;
            $campo->save();
            return Redirect::to('campos')
                    ->with('mensaje_error', 'Campo editado correctamente.');
        }else{
            return Redirect::to('campos')
                    ->with('mensaje_error', 'No tienes permiso para editar ese campo.');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $idUser = DB::table('user_campo')->select('id','id_user')->where('id_campo', '=', $request->campo);
        $idUser = (array) $idUser->get()[0];
        if($idUser['id_user']==$request->user || $request->rol=="admin"){
            Campo::destroy($request->campo);
            DB::table('user_campo')->where('id', $idUser['id'])->delete();
            return Redirect::to('campos')
                    ->with('mensaje_error', 'Campo eliminado correctamente.');
        }else{
            return Redirect::to('campos')
                    ->with('mensaje_error', 'No tienes permiso para eliminar ese campo.');
        }
    }
}
