<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $persona = new User;
        $persona->name = $request->name;
        $persona->username = $request->username;
        $persona->email = $request->email;
        $persona->password = Hash::make($request->password);
        $persona->birthday = $request->birthday;
        $persona->rrss = $request->rrss;
        $persona->last_ip = $request->last_ip;
        $persona->rol = 'user';
        $persona->description = $request->description;
        $persona->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $persona = User::find($id);
        $persona->name = $request->name;
        $persona->username = $request->username;
        $persona->email = $request->email;
        $persona->password = Hash::make($request->password);
        $persona->birthday = $request->birthday;
        $persona->rrss = $request->rrss;
        $persona->last_ip = $request->last_ip;
        $persona->rol = 'user';
        $persona->description = $request->description;
        $persona->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return User::destroy($id);
    }
}
