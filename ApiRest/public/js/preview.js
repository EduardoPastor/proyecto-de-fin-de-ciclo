function readFile(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            var previewZone = document.getElementById('avatar');
            previewZone.src = e.target.result;
        }

        reader.readAsDataURL(input.files[0]);
    }
}

var fileUpload = document.getElementById('file');
fileUpload.onchange = function(e) {
    readFile(e.srcElement);
}