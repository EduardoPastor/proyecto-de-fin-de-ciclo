$(function() {
    var idPartida;
    $("button").click(function() {
        idPartida = this.id
        $.ajax({
            type: "POST",
            url: "/inscribirse",
            data: {
                "_token": token,
                'idUser': idUser,
                'idPartida': idPartida
            },
            success: function(data) {
                if (data) {
                    var html = $('#jugadores' + idPartida + "").html().split('/');
                    html[0]++;
                    $('#jugadores' + idPartida + "").html(html[0] + '/' + html[1]);
                    $("#" + idPartida).html('Desapuntarse');
                } else {
                    var html = $('#jugadores' + idPartida + "").html().split('/');
                    html[0]--;
                    $('#jugadores' + idPartida + "").html(html[0] + '/' + html[1]);
                    $("#" + idPartida).html('Apuntarse');
                }

            }
        });
    });
});