<?php

use Illuminate\Database\Seeder;

class ChatterTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        // CREATE THE CATEGORIES

        \DB::table('chatter_categories')->delete();

        \DB::table('chatter_categories')->insert([
            0 => [
                'id'         => 1,
                'parent_id'  => null,
                'order'      => 1,
                'name'       => 'General',
                'color'      => '#3498DB',
                'slug'       => 'General',
                'created_at' => null,
                'updated_at' => null,
            ],
            1 => [
                'id'         => 2,
                'parent_id'  => null,
                'order'      => 2,
                'name'       => 'Compra-Venta',
                'color'      => '#2ECC71',
                'slug'       => 'Compra-Venta',
                'created_at' => null,
                'updated_at' => null,
            ],
            2 => [
                'id'         => 3,
                'parent_id'  => 1,
                'order'      => 3,
                'name'       => 'Youtube',
                'color'      => '#9B59B6',
                'slug'       => 'Youtube',
                'created_at' => null,
                'updated_at' => null,
            ],
            3 => [
                'id'         => 4,
                'parent_id'  => 1,
                'order'      => 4,
                'name'       => 'Replicas',
                'color'      => '#9B59B6',
                'slug'       => 'Replicas',
                'created_at' => null,
                'updated_at' => null,
            ]
        ]);
    }
}
