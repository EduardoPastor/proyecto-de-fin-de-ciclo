<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKeyToCampo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campo_partida', function (Blueprint $table) {
            $table->foreign('id_partida')->references('id')->on('partidas')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('id_campo')->references('id')->on('campos')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campo-partida', function (Blueprint $table) {
            //
        });
    }
}
