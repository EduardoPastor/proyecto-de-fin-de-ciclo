<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('login');
// });

Route::get('/login', 'AuthController@showLogin');
Route::post('/login', 'AuthController@login');
Route::group(array('before' => 'auth'), function(){
    Route::get('/', 'AuthController@showLogin');
    Route::get('/logout', 'AuthController@logOut');
});
Route::get('/register', 'AuthController@showRegister');
Route::post('/register', 'AuthController@register');
Route::get('/perfil', function () {
    return view('perfil');
});
Route::post('/perfil', 'AuthController@newPass');
Route::get('perfil/{id}',function($id) {
        return view('usuarios',array('user'=>$id)); 
});
Route::post('/editPerfil', 'AuthController@changeInfo');
Route::get('/editPerfil',function() {
    return abort(404); 
});
Route::get('/image-upload', function () {
    return Redirect::to('perfil');
});
Route::post('/image-upload', 'ImageUploadController@imageUploadUser');
Route::post('/deletePerfil', 'AuthController@deleteUser');
Route::get('/deletePerfil',function() {
    return abort(404); 
});
Route::get('/campos', function () {
     return view('campos');
});
Route::get('/newCampo', function () {
    return view('newCampo');
});
Route::post('/newCampo', 'CampoController@store');
Route::get('editCampo/{id}',function($id) {
    return view('campo',array('campo'=>$id)); 
});
Route::post('/editCampo','CampoController@update');
Route::post('/deleteCampo','CampoController@destroy');
Route::get('/deleteCampo',function() {
    return abort(404); 
});
Route::get('/usuarios',function() {
    return view('adminUsers'); 
});
Route::post('/changeRol', 'AuthController@changeRol');
Route::get('/changeRol',function() {
    return abort(404); 
});
Route::get('/crearPartida/{id}',function($id) {
    return view('newPartida',array('campo'=>$id)); 
});
Route::get('/verPartidas/{id}',function($id) {
    return view('partidas',array('campo'=>$id)); 
});
Route::post('/newPartida', 'PartidaController@store');
Route::get('partida/{idCampo}/{idPartida}',function($idCampo, $idPartida) {
    return view('partida',array('partida'=>$idPartida, 'campo'=>$idCampo)); 
});
Route::post('/inscribirse', 'PartidaController@inscripcion');
Route::get('/inscribirse',function() {
    return abort(404); 
});
Route::post('/desapuntarse', 'PartidaController@desapuntarse');
Route::get('/desapuntarse',function() {
    return abort(404); 
});
Route::get('editPartida/{idCampo}/{idPartida}',function($idCampo, $idPartida) {
    return view('editPartida',array('partida'=>$idPartida, 'campo'=>$idCampo)); 
});
Route::post('/editPartida', 'PartidaController@update');
Route::get('/eliminarPartida/{idCampo}','PartidaController@destroy');
Route::post('/eliminarPartida/{idCampo}',function() {
    return abort(404); 
});
Route::get('/misPartidas',function() {
    return view('misPartidas'); 
});
Route::post('/misPartidas',function() {
    return abort(404); 
});