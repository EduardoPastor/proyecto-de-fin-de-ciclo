@extends('layouts.app')

@section('titulo')
    Partidas
@endsection
<?php
    $partidas=DB::table('campo_partida')->get()->where('id_campo', '=', $campo);
?>
@section('content')
<link href="{{asset('css/campos.css')}}" rel='stylesheet' type='text/css'>
<script src="{{asset('js/campos.js')}}"></script>
<script>
    var idUser = "{{ Auth::user()->id }}"
    var token = "{{ csrf_token() }}"
</script>
<script src="{{asset('js/inscribirse.js')}}"></script>
<div class="container">
	<div class="row">
        <div class="col-lg-4 text-center center-block">
              <h5>
              @if(Session::has('mensaje_error'))
                    <div class="alert alert-info">{{ Session::get('mensaje_error') }}</div>
                @endif
            </h5>
        </div>
        <a href="/campos" class="btn btn-lg btn-success pull-right btn-edit"> Volver</a>
        @if(sizeof($partidas)!=0)
            @foreach($partidas as $idPartida)
                <?php
                    $partidas=DB::select("SELECT * FROM partidas WHERE id=".$idPartida->id_partida." AND date >= CAST('".date("Y-m-d")."' AS datetime)");
                ?>
                    @foreach($partidas as $partida)
                        <?php
                            $jugadores= DB::select('SELECT COUNT(id) as numero FROM user_partida WHERE id_partida  = "' . $partida->id .'"');
                            $jugadores = (array) $jugadores[0];
                            $apuntado= DB::select('SELECT COUNT(id) as numero FROM user_partida WHERE id_partida  = "' . $partida->id .'" && id_user = "'. Auth::user()->id .'"');
                            $apuntado = (array) $apuntado[0];
                        ?>
                        <div class="searchable-container">
                            <div class="items col-xs-12 col-sm-12 col-md-12 col-lg-12 clearfix">
                            <div class="info-block block-info clearfix">
                                    <?php
                                        $idUser = DB::table('user_campo')->select('id_user')->where('id_campo', '=', $campo);
                                        $idUser = (array) $idUser->get()[0];
                                    ?>
                                    <div class="pull-left">
                                    @if($partida->photo)
                                        <img src="/{{$partida->photo}}" class="avatar img-circle img-thumbnail" alt="avatar">
                                    @else
                                        <img src="{{asset('img/defaultAvatar.png')}}" class="avatar img-circle img-thumbnail" alt="avatar">
                                    @endif
                                    </div>
                                    <h4>{{$partida->name}}</h4>
                                    <h5>Tipo de partida: {{$partida->type}}</h5>
                                    <p>Fecha: {{$partida->date}}</p>
                                    <div>Jugadores: <span id="jugadores{{$partida->id}}">{{$jugadores['numero']}}/{{$partida->max_players}}</span></div>
                                    <div class="col-4 clearfix">
                                        @if(Auth::guest()==false && (Auth::User()->rol == 'admin' || (Auth::User()->rol == 'campo' && Auth::User()->id == $idUser['id_user'])))
                                            <a href="{{ url('/editPartida').'/'.$campo.'/'.$partida->id }}" class="btn btn-lg btn-success pull-right btn-edit">Editar Partida</a>
                                            <a href="{{ url('/eliminarPartida').'/'.$partida->id }}" class="btn btn-lg btn-success pull-right btn-edit">Eliminar Partida</a>
                                        @endif
                                        <a href="{{ url('/partida').'/'.$campo.'/'.$partida->id }}" class="btn btn-lg btn-success pull-right btn-edit"> Ver detalles</a>
                                        @if(($jugadores['numero'] != $partida->max_players) && ($apuntado['numero']==0))
                                        <button class="btn btn-lg btn-success pull-right btn-edit" type="submit" id="{{ $partida->id }}">Apuntarse</button>
                                        @else
                                        @if($apuntado['numero']==1)
                                        <button class="btn btn-lg btn-success pull-right btn-edit" type="submit" id="{{ $partida->id }}">Desapuntarse</button>
                                        @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
            @endforeach
        @else
            <div class="searchable-container">
                <div class="items col-xs-12 col-sm-12 col-md-12 col-lg-12 clearfix">
                    <div class="info-block block-info clearfix">
                        <h2>No hay partidas en este campo.</h2>    
                    </div>
                </div>
            </div>
        @endif
	</div>
</div>
@endsection