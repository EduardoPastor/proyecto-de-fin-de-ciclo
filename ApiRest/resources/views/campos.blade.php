@extends('layouts.app')

@section('titulo')
    Campos
@endsection
<?php
    $campos=App\Campo::all()
?>

@section('content')
<link href="{{asset('css/campos.css')}}" rel='stylesheet' type='text/css'>
<script src="{{asset('js/campos.js')}}"></script>
<div class="container">
	<div class="row">
        <div class="col-lg-4">
            <h2>Campos de Airsoft</h2>
        </div>
        <div class="col-lg-4 text-center center-block">
              <h5>
              @if(Session::has('mensaje_error'))
                    <div class="alert alert-info">{{ Session::get('mensaje_error') }}</div>
                @endif
            </h5>
        </div>
        @if(Auth::guest()==false && (Auth::User()->rol == 'admin' || Auth::User()->rol == 'campo'))
		<div class="col-lg-4">
        <br>
            <a href="{{ url('/newCampo') }}"><button class="btn btn-lg btn-success pull-right" type="submit"> Nuevo campo</button></a>
        </div>
        @endif
        <div class="col-12">
            <input type="search" class="form-control" id="input-search" placeholder="Buscador de campos..." >
        </div>
        @foreach($campos as $campo)
        <?php
            $idUser = DB::table('user_campo')->select('id_user')->where('id_campo', '=', $campo->id);
            $idUser = (array) $idUser->get()[0];
        ?>
        <div class="searchable-container">
            <div class="items col-xs-12 col-sm-12 col-md-12 col-lg-12 clearfix">
               <div class="info-block block-info clearfix">
                    <div class="pull-left">
                        @if($campo->photo)
                            <img src="/{{$campo->photo}}" class="avatar img-circle img-thumbnail" alt="avatar">
                        @else
                        <img src="{{asset('img/img-test.png')}}" class="avatar img-circle img-thumbnail" alt="avatar">
                        @endif
                    </div>
                    <h5>{{$campo->name}}</h5>
                    <h4>{{$campo->street}}, {{$campo->number}}</h4>
                    <p>{{$campo->cp}}, {{$campo->town}}</p>
                    <div class="col-4 clearfix">
                        @if(Auth::guest()==false && (Auth::User()->rol == 'admin' || (Auth::User()->rol == 'campo' && Auth::User()->id == $idUser['id_user'])))
                                <a href="{{ url('/editCampo').'/'.$campo->id }}"><button class="btn btn-lg btn-success pull-right btn-edit" type="submit"> Editar Campo</button></a>
                                <a href="{{ url('/crearPartida').'/'.$campo->id }}"><button class="btn btn-lg btn-success pull-right btn-edit" type="submit"> Crear partida</button></a>
                        @endif
                        <a href="{{ url('/verPartidas').'/'.$campo->id }}"><button class="btn btn-lg btn-success pull-right btn-edit" type="submit"> Ver partidas</button></a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
	</div>
</div>
@endsection