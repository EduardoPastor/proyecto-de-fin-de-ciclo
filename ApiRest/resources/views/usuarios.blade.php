<?php try{ 
    App\User::findOrFail($user);
?>
@extends('layouts.app')
@section('titulo')
    Perfil de {{App\User::findOrFail($user)->username}}
@endsection

@section('content')
<link href="{{asset('css/perfil.css')}}" rel='stylesheet' type='text/css'>
<div class="container bootstrap snippet">
    <div class="row">
          <div class="col-sm-3 text-center center-block"><h1>{{App\User::findOrFail($user)->username}}</h1></div>
    </div>
    <div class="row">
  		<div class="col-sm-3">
      <div class="text-center">
        @if(App\User::findOrFail($user)->photo)
            <img src="/{{App\User::findOrFail($user)->photo}}" class="avatar img-circle img-thumbnail" alt="avatar">
        @else
        <img src="{{asset('img/defaultAvatar.png')}}" class="avatar img-circle img-thumbnail" alt="avatar">
        @endif
        @if(Auth::user()->rol == 'admin')
        <hr>
        <div class="panel panel-default">
            <div class="panel-heading">Cambiar permisos</div>
            <div class="panel-body">
                {{ Form::open(array('url' => '/changeRol')) }}
                        <select class="form-control" name="rol">
                            @if(App\User::findOrFail($user)->rol == "admin")
				            <option value="admin" selected>Administrador</option>
                            <option value="campo">Dueño de campo</option>
                            <option value="user">Usuario</option>
                            @elseif(App\User::findOrFail($user)->rol == "campo")
                            <option value="admin">Administrador</option>
                            <option value="campo" selected>Dueño de campo</option>
                            <option value="user">Usuario</option>
                            @else
                            <option value="admin">Administrador</option>
                            <option value="campo">Dueño de campo</option>
                            <option value="user" selected>Usuario</option>
                            @endif
			            </select>
                        <input id="id" name="id" type="hidden" value="{{$user}}">
                    <div class="form-group"><br>
                    {{ Form::submit('Cambiar', array('class' => 'btn btn-lg btn-success pull-right')) }} 
                    </div>
                {{ Form::close() }}
            </div>
        </div>
        @endif
      </div>
        </div>
        <div class="col-sm-1"></div>
    	<div class="col-sm-8">  
          <div class="tab-content">
            <div class="tab-pane active" id="home">
                  <hr>
                      <div class="form-group">
                          <div class="col-xs-6">
                              <label for="name"><h4><i class="fa fa-user"></i>Nombre</h4></label>
                              {{ Form::text('name',App\User::findOrFail($user)->name, array('class' => 'form-control', 'readonly')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="lastName"><h4>Apellidos</h4></label>
                              {{ Form::text('surnames',App\User::findOrFail($user)->surnames, array('class' => 'form-control', 'readonly')) }}
                          </div>
                      </div>
          
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="email"><h4><i class="fa fa-envelope-o"></i>Correo electrónico</h4></label>
                              {{ Form::email('email',App\User::findOrFail($user)->email, array('class' => 'form-control', 'readonly')) }}
                          </div>
                      </div>
          
                      <div class="form-group">
                          <div class="col-xs-6">
                             <label for="birthday"><h4><i class="fa fa-birthday-cake"></i>Fecha de nacimiento</h4></label>
                              <input type="date" class="form-control" name="birthday" id="birthday" value="{{App\User::findOrFail($user)->birthday}}" readonly>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="twitter"><h4><i class="fa fa-twitter" aria-hidden="true"></i>Twitter</h4></label>
                              {{ Form::text('twitter',App\User::findOrFail($user)->twitter, array('class' => 'form-control', 'readonly')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="facebook"><h4><i class="fa fa-facebook-official" aria-hidden="true"></i>Facebook</h4></label>
                              {{ Form::text('facebook',App\User::findOrFail($user)->facebook, array('class' => 'form-control', 'readonly')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="instagram"><h4><i class="fa fa-instagram" aria-hidden="true"></i>Instagram</h4></label>
                              {{ Form::text('instagram',App\User::findOrFail($user)->instagram, array('class' => 'form-control', 'readonly')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="equipo"><h4><i class="fa fa-users" aria-hidden="true"></i>Equipo</h4></label>
                              {{ Form::text('team',App\User::findOrFail($user)->team, array('class' => 'form-control', 'readonly')) }}
                          </div>
                      </div>
                      <?php
                        if(App\User::findOrFail($user)->rol == "admin") $rol='Administrador';
                        else if(App\User::findOrFail($user)->rol == "campo") $rol='Dueño de campo';
                        else $rol='Usuario'; 
                      ?>
                @if(Auth::user()->rol == 'admin')
                      <div class="form-group">
                          <div class="col-xs-6">
                            <label for="ip"><h4><i class="fa fa-globe" aria-hidden="true"></i>Ultima IP</h4></label>
                              {{ Form::text('ip',App\User::findOrFail($user)->last_ip, array('class' => 'form-control', 'readonly')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-xs-6">
                            <label for="ip"><h4>Permisos</h4></label>
                              {{ Form::text('rol',$rol, array('class' => 'form-control', 'readonly')) }}
                          </div>
                      </div>
                      {{ Form::open(array('url' => '/deletePerfil')) }}
                      <div style="display:none">
                        {{ Form::text('id',$user, array('class' => 'form-control', 'readonly')) }}
                      </div>
                      <div class="form-group">
                        @if(Auth::user()->id != $user)
                          <div class="col-xs-6"><br>
                          <button class="btn btn-lg btn-success pull-right" type="submit"> Eliminar usuario</button>
                          </div>
                        @endif
                          <div class="col-xs-6">
                                <br>
                                  <a href="/usuarios" class="btn btn-lg btn-success pull-right"> Volver</a>
                            </div>
                      </div>
                      {{ Form::close() }}
                @endif
              </div>
              </div>
          </div>
        </div>
    </div>
@endsection
<?php }catch(\Exception $e){ ?>
    @section('titulo')
    404
    @endsection
    @section('content')
    <div class="site">
        <h1>404|<small>Players Not Found</small></h1>
    </div>
    @endsection
<?php } ?>