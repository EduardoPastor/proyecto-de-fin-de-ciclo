<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('titulo') - Airsoft</title>
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type='text/css'>
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    
    
    @yield('css')
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type='text/css'>
</head>
<body id="app-layout">
    <nav class="border-0 navbar-default" style="background-color: #92907C;">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand">
                    <img src="{{asset('img/logo.png')}}" alt="" width="100px" heigth="200px">
                </a>
            </div>
                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Iniciar Sesión</a></li>
                        <li><a href="{{ url('/register') }}"><i class="fa fa-sign-in"></i> Registrarse</a></li>
                    @else
                        <ul class="nav navbar-nav navbar-left">
                            @if (Auth::user()->rol == "admin")
                            <li><a href="{{ url('/usuarios') }}" class="btn btn-outline-success"><i class="fa fa-btn fa-street-view"></i>Usuarios</a></li>
                            @endif
                            <li><a href="{{ url('/forums') }}"  class="btn btn-outline-success"><i class="fa fa-btn fa-comments"></i>Foro</a></li>
                            <li><a href="{{ url('/misPartidas')}}"  class="btn btn-outline-success"><i class="fa fa-btn fa-child"></i>Mis Partidas</a></li>
                            <li><a href="{{ url('/campos') }}"  class="btn btn-outline-success"><i class="fa fa-btn fa-map-signs"></i>Campos</a></li>
                        </ul>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->username }} <span class="caret"></span>
                                @if(Auth::user()->photo)
                                <img class="profilePhoto" alt="avatar" src="/{{Auth::user()->photo}}">
                                @else
                                <img src="{{asset('img/defaultAvatar.png')}}" class="profilePhoto" alt="avatar">
                                @endif
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/perfil') }}"><i class="fa fa-btn fa-user"></i>Editar Perfil</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Cerrar sesión</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
        </div>
        @if (!Auth::guest())
            <div class="collapse navbar-collapse bg-primary" id="app-navbar-collapse">
                
            </div>
        @endif
    </nav>

    @yield('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    @yield('js')
</body>
</html>