@extends('layouts.app')

@section('titulo')
    Campos
@endsection
<?php
    $users=App\User::all()
?>
@if(Auth::user()->rol == "admin")
@section('content')
<link href="{{asset('css/campos.css')}}" rel='stylesheet' type='text/css'>
<script src="{{asset('js/campos.js')}}"></script>
<div class="container">
	<div class="row">
        <div class="col-lg-4">
            <h2>Usuarios</h2>
        </div>
        <div class="col-lg-4 text-center center-block">
              <h5>
              @if(Session::has('mensaje_error'))
                    <div class="alert alert-info">{{ Session::get('mensaje_error') }}</div>
                @endif
            </h5>
        </div>
        <div class="col-lg-12">
            <input type="search" class="form-control" id="input-search" placeholder="Buscador de usuarios..." >
        </div>
        @foreach($users as $user)
        <div class="searchable-container">
            <div class="items col-xs-12 col-sm-12 col-md-12 col-lg-12 clearfix">
               <div class="info-block block-info clearfix">
                    <div class="pull-left">
                    @if($user->photo)
                        <img src="/{{$user->photo}}" class="avatar img-circle img-thumbnail" alt="avatar">
                    @else
                        <img src="{{asset('img/defaultAvatar.png')}}" class="avatar img-circle img-thumbnail" alt="avatar">
                    @endif
                    <?php
                        if($user->rol == "admin") $rol='Administrador';
                        else if($user->rol == "campo") $rol='Dueño de campo';
                        else $rol='Usuario'; 
                    ?>
                    </div>
                    <h5>{{$user->username}}</h5>
                    <h4>{{$user->name ?? 'Nombre'}} {{$user->surnames ?? 'Apellidos'}}</h4>
                    <p>{{$user->email}}</p>
                    <p>Permisos: {{$rol}}</p>
                    <div class="col-4 clearfix">
                        @if(Auth::guest()==false && (Auth::User()->rol == 'admin' || (Auth::User()->rol == 'campo' && Auth::User()->id == $idUser['id_user'])))
                                <a href="{{ url('/perfil').'/'.$user->id }}"><button class="btn btn-lg btn-success pull-right btn-edit" type="submit"> Editar Usuario</button></a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endforeach
	</div>
</div>
@endsection
@else
@section('titulo')
    404
    @endsection
    @section('content')
    <div class="site">
        <h1>404|<small>Page Not Found</small></h1>
    </div>
@endsection
@endif