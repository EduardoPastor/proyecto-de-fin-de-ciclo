@extends('layouts.app')

@section('titulo')
    Perfil
@endsection

@section('content')
<link href="{{asset('css/perfil.css')}}" rel='stylesheet' type='text/css'>
<div class="container bootstrap snippet">
    <div class="row">
          <div class="col-sm-3 text-center center-block"><h1>{{Auth::user()->username}}</h1></div>
          <div class="col-sm-9 text-center center-block">
              <h3>
              @if(Session::has('mensaje_error'))
                    <div class="alert alert-info">{{ Session::get('mensaje_error') }}</div>
                @endif
            </h3>
        </div>
    </div>
    <div class="row">
  		<div class="col-sm-3">
      <div class="text-center">
        @if(Auth::user()->photo)
            <img src="/{{Auth::user()->photo}}" class="avatar img-circle img-thumbnail" alt="avatar" id="avatar">
        @else
        <img src="{{asset('img/defaultAvatar.png')}}" class="avatar img-circle img-thumbnail" alt="avatar" id="avatar">
        @endif
        <hr>
        <form action="/image-upload" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="file" name="image" class="text-center center-block file-upload" id="file"><br>
        <button type="submit" class="btn btn-success">Cambiar imagen</button>
        </form>
        <hr>
        <div class="panel panel-default">
            <div class="panel-heading">Cambiar contraseña</div>
            <div class="panel-body">
                {{ Form::open(array('url' => '/perfil')) }}
                        {{ Form::label('newpass', 'Contraseña nueva') }}
                        {{ Form::password('password', array('class' => 'form-control')) }}
                        {{ Form::label('passwordTwo', 'Repita la contraseña') }}
                        {{ Form::password('passwordTwo', array('class' => 'form-control')) }}
                    <div class="form-group"><br>
                    {{ Form::submit('Cambiar', array('class' => 'btn btn-lg btn-success pull-right')) }} 
                    </div>
                {{ Form::close() }}
            </div>
        </div>
      </div></hr><br>
        </div>
        <div class="col-sm-1"></div>
    	<div class="col-sm-8">  
          <div class="tab-content">
            <div class="tab-pane active" id="home">
                  <hr>
                  {{ Form::open(array('url' => '/editPerfil')) }}
                      <div class="form-group">
                          <div class="col-xs-6">
                              <label for="name"><h4><i class="fa fa-user"></i>Nombre</h4></label>
                              {{ Form::text('name',Auth::user()->name, array('class' => 'form-control')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="lastName"><h4>Apellidos</h4></label>
                              {{ Form::text('surnames',Auth::user()->surnames, array('class' => 'form-control')) }}
                          </div>
                      </div>
          
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="email"><h4><i class="fa fa-envelope-o"></i>Correo electrónico</h4></label>
                              {{ Form::email('email',Auth::user()->email, array('class' => 'form-control', 'readonly')) }}
                          </div>
                      </div>
          
                      <div class="form-group">
                          <div class="col-xs-6">
                             <label for="birthday"><h4><i class="fa fa-birthday-cake"></i>Fecha de nacimiento</h4></label>
                              <input type="date" class="form-control" name="birthday" id="birthday" value="{{Auth::user()->birthday}}" readonly>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="twitter"><h4><i class="fa fa-twitter" aria-hidden="true"></i>Twitter</h4></label>
                              {{ Form::text('twitter',Auth::user()->twitter, array('class' => 'form-control')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="facebook"><h4><i class="fa fa-facebook-official" aria-hidden="true"></i>Facebook</h4></label>
                              {{ Form::text('facebook',Auth::user()->facebook, array('class' => 'form-control')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="instagram"><h4><i class="fa fa-instagram" aria-hidden="true"></i>Instagram</h4></label>
                              {{ Form::text('instagram',Auth::user()->instagram, array('class' => 'form-control')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="equipo"><h4><i class="fa fa-users" aria-hidden="true"></i>Equipo</h4></label>
                              {{ Form::text('team',Auth::user()->team, array('class' => 'form-control')) }}
                          </div>
                      </div>
                      <div class="form-group">
                           <div class="col-xs-12">
                                <br>
                              	<button class="btn btn-lg btn-success pull-right" type="submit"> Guardar</button>
                            </div>
                      </div>
                {{ Form::close() }}
              </div>
              </div>
          </div>
        </div>
    </div>
    <script src="{{asset('js/preview.js')}}"></script>
@endsection