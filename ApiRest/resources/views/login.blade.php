
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type='text/css'>
        <link href="{{asset('css/login.css')}}" rel='stylesheet' type='text/css'>
    </head>
    <body>
    <div class="login-page">
        <div class="form">
            <div class="panel-body">
                        @if(Session::has('mensaje_error'))
                            <div class="alert alert-danger">{{ Session::get('mensaje_error') }}</div>
                        @endif
                        @if(Session::has('username'))
                            <div class="alert alert-danger">Registrado correctamente.</div>
                        @endif
                        {{ Form::open(array('url' => '/login')) }}
                            @if(Session::has('username'))
                                <div class="form-group">
                                    {{ Form::label('usuario', 'Nombre de usuario') }}
                                    {{ Form::text('username', Session::get('username'), array('class' => 'form-control')) }}
                                </div>
                            @else
                                <div class="form-group">
                                    {{ Form::label('usuario', 'Nombre de usuario') }}
                                    {{ Form::text('username', '', array('class' => 'form-control')) }}
                                </div>
                            @endif
                            <div class="form-group">
                                {{ Form::label('contraseña', 'Contraseña') }}
                                {{ Form::password('password', array('class' => 'form-control')) }}
                            </div>
                            <div class="modal-body row m-0 p-0">
                                    {{ Form::submit('Entrar', array('class' => 'btn btn-lg btn-success pull-right')) }}
                            </div>
                            <div class="modal-body row m-0 p-0">
                                ¿No tienes cuenta? <a href="/register"> Registrarme</a>
                            </div>
                        {{ Form::close() }}
                    </div>
            </div>
        </div>
    </body>
</html>