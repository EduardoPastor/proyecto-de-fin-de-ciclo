@extends('layouts.app')

@section('titulo')
    Nuevo Campo 
@endsection

@section('content')
<link href="{{asset('css/perfil.css')}}" rel='stylesheet' type='text/css'>
<div class="container bootstrap snippet">
    <div class="row">
          <div class="col-sm-3 text-center center-block"></div>
          <div class="col-sm-9 text-center center-block">
              <h3>
              @if(Session::has('mensaje_error'))
                    <div class="alert alert-info">{{ Session::get('mensaje_error') }}</div>
                @endif
            </h3>
        </div>
    </div>
    <div class="row">
  		<div class="col-sm-3">
        {{ Form::open(array('url' => '/newCampo', 'enctype' => 'multipart/form-data')) }}
      <div class="text-center">
        <img src="{{asset('img/img-test.png')}}" class="avatar img-circle img-thumbnail" alt="avatar" id="avatar">
        <hr>
        @csrf
        <input type="file" name="image" class="text-center center-block file-upload" id="file">
      </div></hr><br>
        </div>
        <div class="col-sm-1"></div>
    	<div class="col-sm-8">  
          <div class="tab-content">
            <div class="tab-pane active" id="home">
                  <hr>
                      <div class="form-group">
                          <div class="col-xs-6">
                              <label for="name"><h4><i class="fa fa-user"></i>Nombre del campo</h4></label>
                              {{ Form::text('name','', array('class' => 'form-control')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="lastName"><h4>Población</h4></label>
                              {{ Form::text('town','', array('class' => 'form-control')) }}
                          </div>
                      </div>
          
                      <div class="form-group">
                          
                          <div class="col-xs-9">
                              <label for="email"><h4><i class="fa fa-envelope-o"></i>Calle</h4></label>
                              {{ Form::text('street','', array('class' => 'form-control')) }}
                          </div>
                      </div>
          
                      <div class="form-group">
                          <div class="col-xs-3">
                             <label for="birthday"><h4>Numero</h4></label>
                            {{ Form::number('number','', array('class' => 'form-control', 'min' => '1')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-4">
                              <label for="twitter"><h4></i>Código Postal</h4></label>
                              {{ Form::number('cp','', array('class' => 'form-control', 'min' => '1')) }}
                          </div>
                      </div>
                           <div class="col-xs-12">
                               <br>
                                  <button class="btn btn-lg btn-success pull-right" type="submit"> Guardar</button>
                            </div>
                            <div class="col-xs-12">
                                <br>
                                  <a href="/campos" class="btn btn-lg btn-success pull-right"> Volver</a>
                            </div>
                      </div>
                      <div style="display:none">
                        {{ Form::text('user',Auth::user()->id, array('class' => 'form-control',)) }}
                      </div>
                {{ Form::close() }}
              </div>
              </div>
          </div>
        </div>
    </div>
    <script src="{{asset('js/preview.js')}}"></script>
@endsection