@extends('layouts.app')

@section('titulo')
    Nueva Partida 
@endsection

@section('content')
<link href="{{asset('css/perfil.css')}}" rel='stylesheet' type='text/css'>
<div class="container bootstrap snippet">
    <div class="row">
          <div class="col-sm-3 text-center center-block"></div>
          <div class="col-sm-9 text-center center-block">
              <h3>
              @if(Session::has('mensaje_error'))
                    <div class="alert alert-info">{{ Session::get('mensaje_error') }}</div>
                @endif
            </h3>
        </div>
    </div>
    <div class="row">
  		<div class="col-sm-3">
        {{ Form::open(array('url' => '/newPartida', 'enctype' => 'multipart/form-data')) }}
      <div class="text-center">
        <img src="{{asset('img/img-test.png')}}" class="avatar img-circle img-thumbnail" alt="avatar" id="avatar">
        <hr>
        @csrf
        <input type="file" name="image" class="text-center center-block file-upload" id="file">
      </div></hr><br>
        </div>
        <div class="col-sm-1"></div>
    	<div class="col-sm-8">  
          <div class="tab-content">
            <div class="tab-pane active" id="home">
                  <hr>
                      <div class="form-group">
                          <div class="col-xs-9">
                              <label for="name"><h4>Nombre de la partida</h4></label>
                              {{ Form::text('name','', array('class' => 'form-control')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-xs-3">
                            <label for="lastName"><h4>Fecha</h4></label>
                              {{ Form::date('date',date("Y-m-d"), array('class' => 'form-control', 'min' => date("Y-m-d"))) }}
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-xs-9">
                              <label for="email"><h4>Descripción</h4></label>
                              {{ Form::textarea('description','', array('class' => 'form-control', 'style' => 'resize: none;')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-xs-3">
                             <label for="birthday"><h4>Jugadores máximos</h4></label>
                            {{ Form::number('max_players','1', array('class' => 'form-control', 'min' => '1')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-xs-4">
                              <label for="twitter"><h4></i>Tipo de partida</h4></label>
                              <select name="type" id="type" class='form-control'>
                                <option value="milsim">Milsim</option>
                                <option value="casual" selected>Casual</option>
                              </select>
                          </div>
                      </div>
                           <div class="col-xs-12">
                               <br>
                                  <button class="btn btn-lg btn-success pull-right" type="submit"> Guardar</button>
                            </div>
                            <div class="col-xs-12">
                                <br>
                                  <a href="/campos" class="btn btn-lg btn-success pull-right"> Volver</a>
                            </div>
                      </div>
                      <div style="display:none">
                        {{ Form::text('campo',$campo) }}
                      </div>
                {{ Form::close() }}
              </div>
              </div>
          </div>
        </div>
    </div>
    <script src="{{asset('js/preview.js')}}"></script>
@endsection