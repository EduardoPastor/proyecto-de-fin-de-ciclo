<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Register</title>
        <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
        <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type='text/css'>
        <link href="{{asset('css/login.css')}}" rel='stylesheet' type='text/css'>
    </head>
    <body>
    <div class="login-page">
        <div class="form">
                <div class="panel-body">
                        @if(Session::has('mensaje_error'))
                            <div class="alert alert-danger">{{ Session::get('mensaje_error') }}</div>
                        @endif
                        {{ Form::open(array('url' => '/register')) }}
                            <div class="form-group">
                                {{ Form::label('usuario', 'Nombre de usuario') }}
                                {{ Form::text('username', '', array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('contraseña', 'Contraseña') }}
                                {{ Form::password('password', array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('confirmPassword', 'Repita la contraseña') }}
                                {{ Form::password('confirmPassword', array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('correo', 'Correo Electronico') }}
                                {{ Form::email('email', '', array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('datepicker', 'Fecha de nacimiento') }}
                                <input id="date" type="date" name="date" class="form-control">
                            </div>
                            <div class="modal-body row m-0 p-0">
                                    {{ Form::submit('Registrarse', array('class' => 'btn btn-lg btn-success pull-right')) }} 
                            </div>
                            <div class="modal-body row m-0 p-0">
                                    ¿Tienes ya cuenta? <a href="/login">Inicia Sesion</a>
                            </div>
                        {{ Form::close() }}
                    </div>
            </div>
        </div>
    </body>
     
</html>